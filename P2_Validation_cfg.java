package libs.TestingPlatform;					
import config.ItfMotor.EnmPolarity;
import prj.cte.AMainTaskTS;
public class P2_Validation_cfg {

/*
	Angle configurations
*/
	final public static float IW_POS 							= 20;						//	IW angle
	final public static float OW_POS 							= 150;						//	OW angle
	final public static float SVP_POS 							= 80;						//	Service position angle
	final public static float IP_POS 							= 20;						//	Intermittent park angle 
	final public static float DP_POS 							= 10;						//	Depressed park angle
	final public static float WP_POS 							= 40;						//	Winter park angle
	final public static float AP_POS 							= 40;						//	AP park angle
	final public static float DPH_POS 							= 40;						//	DPH park angle
	final public static float APH_POS 							= 40;						//  APHpark angle
	final public static float IWSnow_POS 						= 40;						//	IW snow load angle
	final public static float OWSnow_POS 						= 135;						//	OW snow load angle
	final public static float IWStall_POS 						= 40;						//	stall load angle
	final public static float OWStall_POS 						= 135;						//	stall  load angle
	final public static float CUST1_POS 							= 20;					//	CUST1 angle
	final public static float CUST2_POS 							= 20;					//	CUST2 angle 
	
	final public static float ARM_FLEXIBILITY_ANGLE             =7 ;                        // angle add to snow load angle the sum of them will be the new snow load angle
	
	final public static int   Angle_tol 						= 3;						//	Angle tolerance
	final public static float HighSpeed_val 					= 60;					//	High speed frequency in CPM
	final public static float LowSpeed_val 						= 40;						//	Low speed frequency in CPM
	final public static int   Speed_tol 						= 3;						//	Speed tolerance in CPM
	final public static int   Stall_POS 						= 3;						//	to set half angle between

	final public static float OW_OFFSET                          =1 ;                              // Ow_Offset angle 
    final public static float IW_OFFSET                         =1 ;                                // IW_offset angle 
	final public static float HighSpeed_HC 						= 0.5f;						//	High speed half cycle time in seconds
	final public static float LowSpeed_HC 						= 0.75f;					//	Low speed half cycle time in seconds
	final public static float HC_tol 							= 0.2f;					//	Half cycle tolerance in seconds 
	final public static int ProtPauseTime						= 1000;						//	Intermittent protection pause time 	//to be set < prot time
	final public static int intermittentPauseTime1				= 1000;						//	Intermittent pause time 1 			//to be set	= prot time
	final public static int intermittentPauseTime2				= 1000;						//	Intermittent pause time 2			//to be set	> prot time
	final public static int intermittentPauseTime3				= 1000;						//	Intermittent pause time 3			//to be set
	final public static int PauseTol							= 1000;						//	pause time Tol						//to be set
	final public static float OVER_Fraction                     =8;                         // over load factor 
	final public static float DRY_CPM_Red                     =15 ;                       // wiping frequency will reduce by this value when over load 
	final public static float DRY_Cycle                        =10 ;                       // number of wipe cycle before enable over load strategy
	final public static float MIN_DRY_CPM                    =30 ;                       // MIN frequency before intermittent mode active while over load 
	final public static float DRY_INTER_TM                    =120 ;                       // time  (S) before intermittent mode active while over load 
   	final public static int DRY_INTER_PAUSE_TM              =8 ;                       // time  (S) pause time of  intermittent mode when over load applied  
   

	/*
motor direction
*/
final public static int OW_Dirrection                        =1;                          //  OW direction
final public static int IW_Dirrection                        =2;                          // Iw direction 

/*
	Voltage management
*/
	final public static int overVoltage_stat 				= 1;						//	Over voltage status value
	final public static int underVoltage_stat 				= 1;						//	Under voltage status value
	final public static int normalVoltage_stat 				= 0;						//	Normal voltage status value
	final public static int UNDIFVoltg_stat                  =1 ;                        // undefined voltage status
	final public static int ResetVoltg_stats                 = 1;                        // reset volatge status
				
	final public static float MaxVolt 							= 16;						//	Over voltage value
	final public static float NominalVolt 						= (float) 13.5;						//	Normal voltage value
	final public static float MinVolt 							= 9;						//	Under voltage value
	final public static float ResetVolt                        =20;                        //  reset voltage value 
	public static final float VoltHyst                          = 1f;                     // voltage hysteresis

/*
    volatge protection component Alaises vlaues
 */
	final public static double MaxVolt2Norl_TM                       =100 ;                      //   time taken to change voltage status from over to normal
	final public static double MinVolt2Norl_TM                       =100 ;                      //   time taken to change voltage status from under to normal
	final public static double NorlVolt2Max_TM                       =100 ;                      //   time taken to change voltage status from normal to over
	final public static double NorlVolt2Min_TM                       =100 ;                      //   time taken to change voltage status from normal to under
	
	/*
		Stall managment:

	*/
	final public static int STALL_RESTART_SEQ_MAX            =5;                      //   Number of break free trial
	final public static int STALL_RESTART_MAX                =5 ;                      //   Number of 1 break free trials
	final public static int STALL_RESTART_TM                 =100 ;                      //   PWM between  each trial
	final public static int STALL_RESTART_SEQ_TM             =100 ;                      //   Time between each group of trails
	final public static int STALL_Trial_TM                    =200 ;                      //   PWM time for each trial
	
	
	
	/*			
	Wiping request management 			
*/ 			
			
	final public static String 	WprSpeed 						= "Sig_WipingSpeed";		//	Wiper speed request signal
	final public static String 	WprSpeed_frame 					= "Tx_WipingSpeed";			//	Wiper speed request frame
	
	final public static String 	IntermittentMode 				= "Sig_IntermittentMode";	//	Wiper request intermittent signal
	final public static String 	IntermittentMode_frame 			= "Tx_IntermittentMode";	//	Wiper request intermittent frame
	
	final public static String	ImmediateChangeRqst				= "ImmediateChange";		//	Immediate change signal
	final public static String	ImmediateChangeRqst_frame		= "Tx_ImmediateChange";		//	Immediate change frame
	
	final public static String	SerivcePosition					= "ServicePositionB";		//	Service position signal
	final public static String	SerivcePosition_frame			= "Tx_ServicePositionB";	//	Service position frame
	
	final public static String	StopPosition					= "StopPosition";		     //	Service position signal
    final public static String	StopPosition_frame				= "TX_StopPosition";		 //	Service position frame
	
	final public static String	MotorDirection					= "motorDiectionSignal";    //	system set this signal to indicate motor direction  
	final public static String	MotorDirection_frame			= "RX_motorDiectionSignal";    //	system set this frame  
	
	final public static String	VehicleSpeed					= "SIG_VehicleSpeed";    	//	Lin Signal for the vehicle speed  
	final public static String	VehicleSpeed_frame				= "TX_VehicleSpeed";    	//	Vehicle speed frame
	
	final public static String	VehicleSpeedValid				= "SIG_VehicleSpeedValid";    	//	Lin Signal for the vehicle speed  
	final public static String	VehicleSpeedValid_frame			= "TX_VehicleSpeedValid";    	//	Vehicle speed frame
	
	final public static String	WipingFreq						= "Sig_WipingFreq";    		//	Wiping Freq Lin signal
	final public static String	WipingFreq_frame				= "TX_WipingFreq";    		//	Wiping Freq Lin frame
	
	final public static String	OverrideTemp					= "Sig_OverrideTemp";    		//	Wiping Freq Lin signal
	final public static String	OverrideTemp_frame				= "TX_OverrideTemp";    		//	Wiping Freq Lin frame
	
	final public static int 	HighSpeedRqst 					= 3;						//	High speed request value
	final public static int 	LowSpeedRqst 					= 1;						//	Low speed request value
	final public static int		Park							= 0;						//	OFF request value
	final public static int		immEn							= 1;						//	Enable immediate change 
	final public static int		immDis							= 0;						//	Disable immediate change
	final public static int		ServiceEn						= 1;						//	Enable service position
	final public static int		ServiceDis						= 0;						//	Disable service position
	final public static int		intermittentOff					= 0;						//	Intermittent off
	final public static int		intermittentPause1				= 1;						//	intermittent pause time 1
	final public static int		intermittentPause2				= 2;						//	intermittent pause time 2
	final public static int		intermittentPause3				= 3;						//	intermittent pause time 3
	
/*
	Error reporting signals
*/
	final public static String	VoltageStatus					= "WMVoltageError";			//	Voltage status signal Over/Under/Normal
	final public static String	VoltageStatus_frame				= "Rx_WMVoltageError";		//	Voltage status frame
	
	final public static String	StallDetected					= "WMStall";				//	Stall detection signal
	final public static String	StallDetected_frame				= "Rx_Stall";				//	Stall detection frame
	
	/*final public static String	StallStop					    = "WMStallStop";			//	Stall stop   signal
	final public static String	StallStop_frame				    = "Rx_StallStop";			//	Stall stop  frame
	
	final public static String	IWSnowLoadDetected			    = "IWSnowLoadDetected";		//	IW Snow Load condition  signal
	final public static String	IWSnowLoadDetected_frame	    = "Rx_IWSnowLoadDetected";	//	IW Snow Load condition frame
	
	final public static String	OWSnowLoadDetected			    = "OWSnowLoadDetected";		//	OW Snow Load condition  signal
	final public static String	OWSnowLoadDetected_frame		= "Rx_OWSnowLoadDetected";	//	OW Snow Load condition frame
	
	final public static String	RestartSequenceFinished	        = "WMStallStop";				//	Restart Sequence Finished   signal
	final public static String	RestartSequenceFinished_frame	= "Rx_StallStop";				//	Restart Sequence Finished  frame
	
	*/
	final public static String	AngleSensorError				= "WMError";				//	Angle sensor error signal
	final public static String	AngleSensorError_frame			= "Rx_Error";				//	Angle sensor error frame
	
	final public static String	VoltageSensorError				= "WMError";				//	Voltage sensor error signal
	final public static String	VoltageSensorError_frame		= "Rx_Error";				//	Voltage sensor error frame
	
	final public static String	TempSensorError					= "WMError";				//	Temperature status signal Over/Normal
	final public static String	TempSensorError_frame			= "Rx_Error";				//	Temperature status frame
	
	final public static String	TemperatureSensorStatus  		= "WMOverTemp";				//	Temperature sensor error signal
	final public static String	TemperatureSensorStatus_frame  	= "Rx_OverTemp";			//	Temperature sensor error frame
	
	
	
/*
	Temperature sensor values
*/

	final public static int 	S1_Limit						= 85;						//	Limit wiping speed to S1 value
	final public static int		INT_Limit						= 88;						//	Limit wiping speed to intermittent mode value
	final public static int		MAX_Limit						= 90;						//	Limit wiping speed to OFF value
	final public static int		Temp_Normal_Volt				= 1700;						//	Voltage Value corresponding to Limit wiping speed to S1 value (mv)
	final public static int		Temp_S1_Volt					= 1499;						//	Voltage Value corresponding to Limit wiping speed to S1 value (mv)
	final public static int		Temp_Intermittent_Volt			= 1420;						//	Voltage Value corresponding to Limit wiping speed to intermittent mode value (mv)
	final public static int		Temp_Off_Volt					= 1200;						//	Voltage Value corresponding to Limit wiping speed to OFF value (mv)
	final public static int		Temp_Off2Intermittent_Volt		= 1420;						//	Voltage Value corresponding to Limit wiping speed to intermittent mode value (mv)
	final public static int		Temp_int2LowSpeed				= 1700;						//	Voltage Value corresponding to Limit wiping speed to S1 value (mv)
	final public static int		Temp_LowtoNormal				= 1800;						//	Voltage Value corresponding to Limit wiping speed to S1 value (mv)
	final public static int		Temp_UNDEF						= 800;						//	Voltage Value corresponding to Limit wiping speed to S1 value (mv)
	
	/*
	Temperature sensor Status
*/
	final public static int Temp_Nominal_Status             =1 ;                         // Temp Sensor value for Nominal temp
	final public static int Temp_Intermittent_Status        =2 ;                         // Temp Sensor value for Nominal temp
	final public static int Temp_OFF_Status                 =3 ;                         // Temp Sensor value for Nominal temp

/*	
	Rain sensor configuration 	
*/	
	final public static String	RainSesorActive					= "RainSensorActiveB";		//	Enable rain sensor mode signal
	final public static String	RainSesorActive_frame			= "Tx_RainSensorActiveB";	//	Enable rain sensor mode frame
	
	final public static String	RainSensorSpeedRqst				= "WipeSpeedRainSensor";	//	Rain sensor wiping speed request signal
	final public static String	RainSensorSpeedRqst_frame		= "Tx_WipeSpeedRainSensor";	//	Rain sensor wiping speed request frame
	
	final public static String	SplashRqst						= "SplashDetected";			//	Enable rain sensor splash signal
	final public static String	SplashRqst_frame				= "Tx_SplashDetected";		//	Enable rain sensor splash frame
		
	final public static int	RSMSpeed1						= 35;						//	Rain sensor speed 1 frequency in CPM 
	final public static int	RSMSpeed2						= 40;						//	Rain sensor speed 2 frequency in CPM 
	final public static int	RSMSpeed3						= 43;						//	Rain sensor speed 3 frequency in CPM 
	final public static int	RSMSpeed4						= 48;						//	Rain sensor speed 4 frequency in CPM 
	final public static int	RSMSpeed5						= 50;						//	Rain sensor speed 5 frequency in CPM 
	final public static int	RSMSpeed6						= 54;						//	Rain sensor speed 6 frequency in CPM 
	final public static int	RSMSpeed7						= 60;						//	Rain sensor speed 7 frequency in CPM
		
	final public static int	Speed1Rqst						= 1;						//	Rain sensor speed 1 request value
	final public static int	Speed2Rqst						= 2;						//	Rain sensor speed 2 request value
	final public static int	Speed3Rqst						= 3;						//	Rain sensor speed 3 request value
	final public static int	Speed4Rqst						= 4;						//	Rain sensor speed 4 request value
	final public static int	Speed5Rqst						= 5;						//	Rain sensor speed 5 request value
	final public static int	Speed6Rqst						= 6;						//	Rain sensor speed 6 request value
	final public static int	Speed7Rqst						= 7;						//	Rain sensor speed 7 request value
		
/*	
	Wash function	
*/	
	final public static String	WashRqst						= "WashRequest";			// Wash request signal
	final public static String	WashStat						= "WashStat";				// Wash status signal
	final public static int		WashEn							= 1;						// Wash request enable value
	final public static int		WashDis							= 0;						// Wash request disbale value
	
	/*	
	Override Temp	
*/	
	
	final public static int		OverrideTempEn					= 1;						// Override Temp protection Enable
	final public static int		OverrideTempDis					= 0;						// Override Temp protection Disable
	
/*
    stop positions 
*/


final public static int AP_POS_RQ                            =1 ;                            // AP_target_postion_lin_signal_value
final public static int IP_POS_RQ                            =2 ;                            // IP_target_postion_lin_signal_value
final public static int DPH_POS_RQ                           =3 ;                            // DPH_target_postion_lin_signal_value
final public static int APH_POS_RQ                           =4;                             // APH_target_postion_lin_signal_value
final public static int DP_POS_RQ                            =5 ;                            // DP_target_postion_lin_signal_value
final public static int WP_POS_RQ                            =5 ;                            // WP_target_postion_lin_signal_value
final public static int SVP_POS_RQ                           =5 ;                            // SVP_target_postion_lin_signal_value
final public static int OW_POS_RQ                            =5 ;                            // OW_target_postion_lin_signal_value
final public static int IW_POS_RQ                            =5 ;                            // IW_target_postion_lin_signal_value
final public static int CUST1_POS_RQ                            =5 ;                         // Custom Target Position 1
final public static int CUST2_POS_RQ                            =5 ;                         // Custom Target Position 2

final public static int Park_AP_POS                          =1 ;                            // Request Park Position: AP
final public static int Park_IP_POS                          =2 ;                            // Request Park Position: IP
final public static int Park_DPH_POS                         =3 ;                            // Request Park Position: DPH
final public static int Park_APH_POS                         =4;                             // Request Park Position: APH
final public static int Park_DP_POS                          =5 ;                            // Request Park Position: DP
final public static int Park_WP_POS                          =6 ;                            // Request Park Position: WP

	/*
    Vehicle Speed 
*/

	final public static int		Vehcile_speed1  				= 0;						//	Vehicle Speed
	final public static int		Vehcile_speed2					= 50;						//	Vehicle Speed
	final public static int		Vehcile_speed3					= 51;						//	Vehicle Speed
	final public static int		Vehcile_speed4					= 99;						//	Vehicle Speed
	final public static int		Vehcile_speed5					= 100;						//	Vehicle Speed
	final public static int		Vehcile_speed6					= 101;						//	Vehicle Speed
	final public static int		Vehcile_speed7					= 199;						//	Vehicle Speed
	final public static int		Vehcile_speed8					= 200;						//	Vehicle Speed
	final public static int		Vehcile_speed9					= 201;						//	Vehicle Speed
	
		/*
    Vehicle Speed Validity
	*/
	final public static int		VSpeedValid						= 1;						//Vehicle Speed validity Valid
	final public static int		VSpeedInValid					= 0;						//Vehicle Speed validity inValid

	/*
	*/
	
	final public static double SW_LOOP_TM                           =10;                                // time within it system will response after wake up  10ms 

	/*
	*/
	final public static int  SNOWCYCLE_MAX                         =5;                               // number of snow load cycle to convert snow load condition to false   


		/*
    Vehicle Speed OW OFFSET
*/

	final public static int		VS_OW_Offset1	 				= 0;						//	Offset angle 1 for Vehicle Speed
	final public static int		VS_OW_Offset2					= -2;						//	Offset angle 2 for Vehicle Speed
	final public static int		VS_OW_Offset3					= -3;						//	Offset angle 3 for Vehicle Speed
	
	
		/*
    Wiping Frequency
*/
	final public static int		Wiping_freq1					= 29;						//	Wiping freq
	final public static int		Wiping_freq2					= 30;						//	Wiping freq
	final public static int		Wiping_freq3					= 31;						//	Wiping freq
	final public static int		Wiping_freq4					= 49;						//	Wiping freq
	final public static int		Wiping_freq5					= 50;						//	Wiping freq
	final public static int		Wiping_freq6					= 51;						//	Wiping freq
	final public static int		Wiping_freq7					= 64;						//	Wiping freq
	final public static int		Wiping_freq8					= 65;						//	Wiping freq
	final public static int		Wiping_freq9					= 66;						//	Wiping freq
	
		/*
   Wipinf Freq  OW OFFSET
*/

	final public static int		WF_OW_Offset1	 				= 0;						//	Offset angle 1 for Wip freq
	final public static int		WF_OW_Offset2					= -1;						//	Offset angle 2 for Wip freq
	final public static int		WF_OW_Offset3					= -3;						//	Offset angle 3 for Wip freq
	
	/*
   Wipinf Freq  IW OFFSET
*/

	final public static int		WF_IW_Offset1	 				= 0;						//	Offset angle 1 for Wip freq
	final public static int		WF_IW_Offset2					= -1;						//	Offset angle 2 for Wip freq
	final public static int		WF_IW_Offset3					= -3;						//	Offset angle 3 for Wip freq
	
	public static float OV_IP_HCT=	LowSpeed_HC;
	
		
	final public static int 	StallTrue						=1;							// Stall SIgnal value for Stall detected
	final public static int 	StallFalse						=0;							// Stall SIgnal value for Stall Not detected
	public void task() {
	
	 OV_IP_HCT= ((P2_Validation_cfg.OW_POS-P2_Validation_cfg.IP_POS)*P2_Validation_cfg.LowSpeed_HC)/(P2_Validation_cfg.OW_POS-P2_Validation_cfg.IW_POS);
	}
	
	

}
