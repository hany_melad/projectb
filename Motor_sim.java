package libs.TestingPlatform;

import config.ETS;
import config.ItfMotor.EnmDirPolarity;
import config.ItfMotor.EnmIWOW_Check;

public class Motor_sim {

 static int  motor_poarity=0;  // change polarity to 0 if motor polarity is negative 1 if motor polarity positive
	
	
	public static void evtDirectionChangeOW(ETS exu,float time_out){
	if (motor_poarity==1)
	{
		exu.pauseUntilEvent(exu.modTS().objMotorSim().evtDirectionChangeOW(),time_out/*time out */ );
	}
	else {
		    exu.comment("API Exhanged ");
	
		exu.pauseUntilEvent(exu.modTS().objMotorSim().evtDirectionChangeIW(),time_out /*time out */);
	
	}
	}
	public static void evtDirectionChangeIW(ETS exu,float time_out){
	if (motor_poarity==1)
	{
		exu.pauseUntilEvent(exu.modTS().objMotorSim().evtDirectionChangeIW(),time_out );
	}
	else {
	    exu.comment("API Exhanged ");
		exu.pauseUntilEvent(exu.modTS().objMotorSim().evtDirectionChangeOW(),time_out );
	
	}
	}
	public static void afcCheck_Motor_Direction(ETS exu,EnmDirPolarity _Direction){
	if (motor_poarity==1)
	{
		    exu.modTS().objMotorSim().afcCheck_Motor_Direction(_Direction);
	
	}
	else {
		    exu.comment("API Exhanged ");
	
			if(_Direction.toInt()==1)
					exu.modTS().objMotorSim().afcCheck_Motor_Direction(EnmDirPolarity.IW);
			else 
			    exu.modTS().objMotorSim().afcCheck_Motor_Direction(EnmDirPolarity.OW);
			
	}
	}
   public static void afcCheck_Motor_Cycle_Time(ETS exu,float _Half_Cycle_Time, float _Time_Tolerance, EnmIWOW_Check _Angle_Check, float _IW_Angle, float _OW_Angle, float _Angle_Tolerance){
	if (motor_poarity==1)
	{
         exu.modTS().objMotorSim().afcCheck_Motor_Cycle_Time(_Half_Cycle_Time,_Time_Tolerance,_Angle_Check,_IW_Angle,_OW_Angle,_Angle_Tolerance);
         	}
	else {
		    exu.comment("API Exhanged ");
	
         exu.modTS().objMotorSim().afcCheck_Motor_Cycle_Time(_Half_Cycle_Time,_Time_Tolerance,_Angle_Check,_OW_Angle,_IW_Angle,_Angle_Tolerance);
	
	}
    }

}
