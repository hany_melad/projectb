package libs.TestingPlatform;

import prj.cte.AMainTaskTS;
import config.ETS;
import config.ItfDAC.EnmDacChnls;
import config.ItfMotor.EnmDirPolarity;
import config.ItfMotor.EnmIWOW_Check;
import config.ItfMotor.EnmMachine_Constants;
import config.ItfMotor.EnmPolarity;
import libs.c5.interfaces.C5;
import libs.c5.interfaces.C5.PowerSupply.SupplyID;
import libs.c5.interfaces.C5.PowerSupply.SupplyState;
import prj.cte.LinNetwork3;


public class Init_Deinit_ECU {
	// TODO add all Initializations and Deinitializations for ECU and it's
	// prefered to use Exu instead of C5 as All channels definitions not
	// supported by C5.

	/********************
	 * * STD VTP Tests Init function *
	 ************************/
	public static void Init_ECU(ETS exu) {
		// TODO add your implementation for Init Function
		C5.Util.comment("set power supply channel to nominal voltage and set power supply current limit");
		C5.Util.comment("Turn on power supply channel 1");

		C5.PowerSupply.selectChannel(SupplyID.S1);
		C5.PowerSupply.setOutput(SupplyState.Off);
		C5.pause(150);
		C5.PowerSupply.setVoltage(13.5);
		C5.pause(150);
		C5.PowerSupply.setChannelCurrentLimit(8);
		C5.pause(15);
		C5.PowerSupply.setOutput(SupplyState.On);
		exu.modTS().objLinMaster().afcConfigureBaudRate(9600);
		exu.pause(300);
		exu.modTS().objLinMaster().afcSetStartSchedulingDelay(200);
		exu.pause(300);

		//exu.modTS().objLinMaster().afcStartScheduling(false);
		//exu.pause(300);
		LinNetwork3.Table_WiperMotorModule_Mode_L6.activate();
		//exu.pause(300);
		//exu.modTS().objLinMaster().afcSetStartSchedulingDelay(200);
		exu.pause(300);
	//	exu.modTS().LinNetwork3.WakeUp();
		LinNetwork3.startScheduling();
		
		//exu.pause(1000);
		//exu.modTS().objLinMaster().afcEventNoSlaveDataConfig(false);
		exu.pause(1000);
		exu.modTS().objMotorSim().afcStart_Simulation();
		exu.pause(500);
		exu.modTS().objMotorSim().afcSet_Machine_Constant(EnmMachine_Constants.M0c_St, 0);
		exu.modTS().objMotorSim().afcSet_Machine_Constant(EnmMachine_Constants.M0c_D, 0);
		exu.modTS().objMotorSim().afcSet_Machine_Constant(EnmMachine_Constants.M0c_Sl, 0);
		exu.modTS().objMotorSim().afcSet_Machine_Constant(EnmMachine_Constants.I, 69);
		exu.modTS().objMotorSim().afcSet_Machine_Constant(EnmMachine_Constants.Ke, 1.595);
		exu.modTS().objMotorSim().afcSet_Machine_Constant(EnmMachine_Constants.Kc, 1.6);
		exu.modTS().objMotorSim().afcSet_Machine_Constant(EnmMachine_Constants.L, 0.000181);
		exu.modTS().objMotorSim().afcSet_Machine_Constant(EnmMachine_Constants.R, 0.131);
		exu.modTS().objMotorSim().afcSet_Machine_Constant(EnmMachine_Constants.J, 0.0000405);
		exu.modTS().objMotorSim().afcSet_Machine_Constant(EnmMachine_Constants.Load, 0);
		exu.modTS().objMotorSim().afcSet_Machine_Constant(EnmMachine_Constants.W_Max, 10);
		exu.modTS().objMotorSim().afcSet_Machine_Constant(EnmMachine_Constants.W_Min, 8);
		exu.modTS().objMotorSim().afcSet_Angle_Polarity(EnmPolarity.Negative);

		exu.modTS().objMotorSim().afcSet_Motor_Angle_Offset_MLX(30);
		exu.modTS().objMotorSim().afcSet_Motor_Angle_Offset_Graph(310);
		exu.modTS().objMotorSim().afcSet_Stall_Angle_1(false, 0, 1, 0);
		exu.modTS().objMotorSim().afcSet_Stall_Angle_2(false, 0, 1, 0);
		exu.modTS().objMotorSim().afcSet_Stall_Angle_3(false, 0, 1, 0);
		exu.modTS().objMotorSim().afcSet_Stall_Angle_4(false, 0, 1, 0);
		
		exu.comment("Set Signal (Terminal_XB,1);");
		LinNetwork3.Tx_BCM_L6_C.Sig_Terminal_XB.set(1);

		
		exu.comment(" Set Signal (Terminal_15B,1);");
		LinNetwork3.Tx_BCM_L6_C.Sig_Terminal_15B.set(1);
		
		LinNetwork3.Tx_BCM_L6_C.Sig_RainSensorActiveB.set(0);
		LinNetwork3.Tx_BCM_L6_E.Sig_StopPosition.set(0);
		exu.pause(200);
		LinNetwork3.Tx_BCM_L6_C.Sig_ServicePosition.set(0);
		C5.pause(1500);

		LinNetwork3.Tx_BCM_L6_E.Sig_WipingSpeed.set(3);
		LinNetwork3.Tx_BCM_L6_E.Sig_StopPosition.set(0);
		C5.pause(500);
		LinNetwork3.Tx_BCM_L6_E.Sig_WipingSpeed.set(0);

		C5.pause(2000);
		exu.modTS().objMotorSim().afcCheck_Angle_Range(8, 13);
		exu.modTS().objDAC().afcsetVoltage(1700, EnmDacChnls.DAC_Ch_01);
		exu.modTS().objDAC().afcsetVoltage(4000, EnmDacChnls.DAC_Ch_02);
		exu.modTS().objDAC().afcsetVoltage(5000, EnmDacChnls.DAC_Ch_03);
	
		exu.modTS().objMotorSim().afcCheck_Angle_Range(8, 13);
		LinNetwork3.Tx_BCM_L6_E.Sig_StopPosition.set(0);
		exu.modTS().objMotorSim().afcCheck_Angle_Range(8, 13);
		exu.modTS().objMotorSim().afcCheck_Angle_Range(8, 13);
		exu.modTS().objMotorSim().afcCheck_Angle_Range(8, 13);
		exu.modTS().objMotorSim().afcCheck_Angle_Range(8, 13);
		
    	exu.modTS().objMotorSim().evtDirectionChangeOW();
		
		
	}
	
 static int  motor_poarity=1;  // change polarity to 0 if motor polarity is negative 1 if motor polarity positive
	
	
	public static void evtDirectionChangeOW(ETS exu,float time_out){
	if (motor_poarity==1)
	{
		exu.pauseUntilEvent(exu.modTS().objMotorSim().evtDirectionChangeOW(),time_out/*time out */ );
	}
	else {
		    exu.comment("API Exhanged ");
	
		exu.pauseUntilEvent(exu.modTS().objMotorSim().evtDirectionChangeIW(),time_out /*time out */);
	
	}
	}
	public static void evtDirectionChangeIW(ETS exu,float time_out){
	if (motor_poarity==1)
	{
		exu.pauseUntilEvent(exu.modTS().objMotorSim().evtDirectionChangeIW(),time_out );
	}
	else {
	    exu.comment("API Exhanged ");
		exu.pauseUntilEvent(exu.modTS().objMotorSim().evtDirectionChangeOW(),time_out );
	
	}
	}
	public static void afcCheck_Motor_Direction(ETS exu,EnmDirPolarity _Direction){
	if (motor_poarity==1)
	{
		    exu.modTS().objMotorSim().afcCheck_Motor_Direction(_Direction);
	
	}
	else {
		    exu.comment("API Exhanged ");
	
			if(_Direction.toInt()==1)
					exu.modTS().objMotorSim().afcCheck_Motor_Direction(EnmDirPolarity.IW);
			else 
			    exu.modTS().objMotorSim().afcCheck_Motor_Direction(EnmDirPolarity.OW);
			
	}
	}
   public static void afcCheck_Motor_Cycle_Time(ETS exu,float _Half_Cycle_Time, float _Time_Tolerance, EnmIWOW_Check _Angle_Check, float _IW_Angle, float _OW_Angle, float _Angle_Tolerance){
	if (motor_poarity==1)
	{
         exu.modTS().objMotorSim().afcCheck_Motor_Cycle_Time(_Half_Cycle_Time,_Time_Tolerance,_Angle_Check,_IW_Angle,_OW_Angle,_Angle_Tolerance);
         	}
	else {
		    exu.comment("API Exhanged ");
	
         exu.modTS().objMotorSim().afcCheck_Motor_Cycle_Time(_Half_Cycle_Time,_Time_Tolerance,_Angle_Check,_OW_Angle,_IW_Angle,_Angle_Tolerance);
	
	}
    }
	
	
	
	/******************
	 * * STD VTP Tests Deinit function *
	 ************************/
	public static void Deinit_ECU(ETS exu) {
		// TODO add your implementation for Deinit Function

	}
}
